/**
 * 参数存储,缓存
 * @type {{set(*, *): void, get(*): *, del(*): void, clear(): void}}
 */
const store = {
  set(key, value) {  // 添加参数
    if (window.localStorage.getItem('superapi_store') == null) {
      window.localStorage.setItem('superapi_store', '{"version":"1.0"}')
    }
    if (window.localStorage.getItem('superapi_store') != null) {
      var tempParamObj = JSON.parse(window.localStorage.getItem('superapi_store'))
      tempParamObj[key] = value
      window.localStorage.setItem('superapi_store', JSON.stringify(tempParamObj))
    }
  },
  get(key) { // 获取参数
    let tempParamObj = JSON.parse(window.localStorage.getItem('superapi_store'))
    if (tempParamObj == null) {
      return undefined
    } else {
      return tempParamObj[key]
    }
  },
  del(key) { // 删除参数
    if (window.localStorage.getItem('superapi_store') != null) {
      var tempParamObj = JSON.parse(window.localStorage.getItem('superapi_store'))
      delete tempParamObj[key]
      window.localStorage.setItem('superapi_store', JSON.stringify(tempParamObj))
    }
  },
  clear() { // 清空
    window.localStorage.removeItem('superapi_store')
  }
}

export default store
