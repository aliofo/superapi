# superapi
在线接口文档管理系统

让写文档更简单快速，让后台开发人员在测试接口的时候就把文档写好补全，让前端开发人员更清晰实时的看接口文档。

在闲余时间会继续开发一些功能，喜欢的话就顺手给个星吧！~

[后台传送门](https://gitee.com/tianyu1993/superapi-admin)

### 使用说明
1. 装nodejs
2. 在项目根目录执行命令 `npm install`
3. 开发运行命令 `npm run dev` 打包运行命令 `npm run build`

**dist文件夹下有打包好的**

### 主要功能
1. 用户管理：用户注册，登录
2. 项目管理：新增编辑项目，新增编辑项目成员
3. 接口管理：新增模块，新增接口
4. 接口测试：填写模拟参数，接口测试
5. 接口文档：简洁清晰的界面，支持接口测试


### 后台
springboot + mybatis + mysql

### 前台
vue + vue router + webpack + sass + element ui

### 部分截图

![image](http://tianyuyun.qiniudn.com/superapi4.png)
![image](http://tianyuyun.qiniudn.com/superapi5.png)
![image](http://tianyuyun.qiniudn.com/superapi6.png)
![image](http://tianyuyun.qiniudn.com/superapi7.png)
![image](http://tianyuyun.qiniudn.com/superapi8.png)
![image](http://tianyuyun.qiniudn.com/superapi10.png)
![image](http://tianyuyun.qiniudn.com/superapi12.png)
![image](http://tianyuyun.qiniudn.com/superapi1.png)
![image](http://tianyuyun.qiniudn.com/superapi2.png)
![image](http://tianyuyun.qiniudn.com/superapi3.png)
